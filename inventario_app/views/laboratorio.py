import json
import traceback
import calendar
from datetime import  date, datetime, time, timedelta
from itertools import chain
from .__init__ import *
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template.loader import get_template,render_to_string
from django.utils.html import strip_tags
from django.template import Context
from django.contrib import messages
from django.contrib.auth import login
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import Avg, Count, Sum, Min, Max, Func
from django.contrib.auth.models import User
from django.forms import inlineformset_factory
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.http import JsonResponse,HttpResponse, HttpResponseRedirect, HttpRequest
from django.views.generic import (TemplateView,CreateView, DeleteView, DetailView, ListView,
                                  UpdateView, TemplateView, View)
from ..decorators import laboratorio_required
                                  
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from rest_framework.views import APIView
from rest_framework.response import Response
from inventario_app.forms import LaboratorioSignUpForm, CrearInventarioForm, CrearCaracteristicasForm, CrearMonitorForm, IncidenciaForm, ReporteForm, ReporteNewForm
from inventario_app.models import Inventario, Marca, User, Equipo, Administrador, Colegios, Adicional, Monitor, Incidencia, Reporte, ReporteNew
# Create your views here.

class LaboratorioSignUpView(CreateView):
    model = User
    form_class = LaboratorioSignUpForm
    template_name = 'registration/signup_form.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'Laboratorio'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('laboratorio:index')

def index(request):
    
    laboratorio = request.user.laboratorio
    colegio_laboratorio = laboratorio.colegio.values_list('pk', flat=True)
    inventario = Inventario.objects.filter(colegio__in=colegio_laboratorio).select_related('colegio')

    return render(request, "inventario_app/laboratorio/index.html", {'inventario':inventario})

@method_decorator([login_required, laboratorio_required], name='dispatch')
class CrearInventarioView(CreateView):
    model = Inventario
    form_class = CrearInventarioForm
    template_name='inventario_app/laboratorio/crearinventario.html'

    def form_valid(self, form):
        inventario = form.save(commit=False)
        inventario.user = self.request.user
        inventario.save()
        messages.success(self.request, 'Inventario agregado')
        return redirect('laboratorio:inventario_detalle', inventario.pk)



@login_required
@laboratorio_required
def crear_adicional(request, pk):
    inventario = get_object_or_404(Inventario, pk=pk)

    if request.method == 'POST':
        form = CrearCaracteristicasForm(request.POST,  request.FILES)
        if form.is_valid():
            adicional = form.save(commit=False)
            adicional.inventario = inventario
            adicional.save()
            messages.success(request, 'Adicional creado')
            return redirect('laboratorio:inventario_detalle', inventario.pk)
    else:
        form = CrearCaracteristicasForm()
    return render(request, 'inventario_app/laboratorio/crear_adicional.html',{'inventario':inventario,'form':form})



@login_required
@laboratorio_required
def crear_monitor(request, pk):
    inventario = get_object_or_404(Inventario, pk=pk)

    if request.method == 'POST':
        form = CrearMonitorForm(request.POST,  request.FILES)
        if form.is_valid():
            monitor = form.save(commit=False)
            monitor.inventario = inventario
            monitor.save()
            messages.success(request, 'Monitor creado')
            return redirect('laboratorio:inventario_detalle', inventario.pk)
    else:
        form = CrearMonitorForm()
    return render(request, 'inventario_app/laboratorio/crear_monitor.html',{'inventario':inventario,'form':form})



@login_required
@laboratorio_required
def crear_incidencia(request, pk):
    inventario = get_object_or_404(Inventario, pk=pk)

    if request.method == 'POST':
        form = IncidenciaForm(request.POST,  request.FILES)
        if form.is_valid():
            incidencia = form.save(commit=False)
            incidencia.inventario = inventario
            incidencia.save()
            messages.success(request, 'Incidencia creada')
            return redirect('laboratorio:inventario_detalle', inventario.pk)
    else:
        form = IncidenciaForm()
    return render(request, 'inventario_app/laboratorio/crear_incidencia.html',{'inventario':inventario,'form':form})

@login_required
@laboratorio_required
def crear_reporte(request):

    if request.method == 'POST':
        form = ReporteForm(request.POST,  request.FILES)
        if form.is_valid():
            reporte = form.save(commit=False)
            reporte.user = request.user
            reporte.save()
            messages.success(request, 'Reporte creado')
            return redirect('laboratorio:reportes')
    else:
        form = ReporteForm()
    return render(request, 'inventario_app/laboratorio/crear_reporte.html',{'form':form})

@login_required
@laboratorio_required
def crear_reporte_new(request, pk):
    reporte = get_object_or_404(Reporte, pk=pk)
    if request.method == 'POST':
        form = ReporteNewForm(request.POST,  request.FILES)
        if form.is_valid():
            reportenew = form.save(commit=False)
            reportenew.reporte = reporte
            reportenew.user = request.user
            reportenew.save()
            messages.success(request, 'Reporte creado')
            return redirect('laboratorio:reportes_view', reporte.pk)
    else:
        form = ReporteNewForm()
    return render(request, 'inventario_app/laboratorio/crear_reporte_new.html',{ 'reporte':reporte, 'form':form})

@login_required
@laboratorio_required
def descripcion(request, pk):
    inventario = get_object_or_404(Inventario, pk=pk)
    adicional = Adicional.objects.filter(inventario=inventario)
    monitor = Monitor.objects.filter(inventario=inventario)
    incidencia = Incidencia.objects.filter(inventario=inventario)
    return render(request, "inventario_app/laboratorio/descripcion.html", {'inventario':inventario, 'adicional':adicional,'monitor':monitor,'incidencia':incidencia })

@login_required
@laboratorio_required
def reportes(request):
    reporte = Reporte.objects.filter(user=request.user)
    date = datetime.now()
    hora = date.strftime("%H:%M:%S")
    return render(request, "inventario_app/laboratorio/reportes.html", {'reporte':reporte, 'hora':hora })

@login_required
@laboratorio_required
def reportes_view(request, pk):
    reporte = get_object_or_404(Reporte, pk=pk)
    reporte_new = ReporteNew.objects.filter(reporte=reporte).order_by('-tiempo')

    return render(request, "inventario_app/laboratorio/reporte_view.html", {'reporte':reporte, 'reporte_new':reporte_new})