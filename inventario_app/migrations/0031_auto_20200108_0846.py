# Generated by Django 3.0 on 2020-01-08 11:46

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('inventario_app', '0030_auto_20200106_0846'),
    ]

    operations = [
        migrations.AddField(
            model_name='despacho',
            name='fecha',
            field=models.DateField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='historicaldespacho',
            name='fecha',
            field=models.DateField(blank=True, default=django.utils.timezone.now, editable=False),
            preserve_default=False,
        ),
    ]
